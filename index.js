//bsconfiglibrary.js

var request   = require('request')
var log4js  = require('log4js')

var logger  = log4js.getLogger()

function BSConfigLibrary(host) {
  this.host = host
}

BSConfigLibrary.prototype.setHost = function(host) {
  this.host = host
}

BSConfigLibrary.prototype.getHost = function() {
  return this.host
}

BSConfigLibrary.prototype.getConfig = function(configPath) {

  return new Promise((resolve, reject) => {

    request.get({
      url: this.host + configPath,
      json: true
    }, function (error, response, body) {

      logger.debug('get response:'+JSON.stringify(response))
      if (!error && response.statusCode === 200) {
        logger.debug('StatusCode 200')
        resolve(body)
      } else if (!error && response.statusCode === 404) {
        logger.debug('StatusCode 404')
        resolve(body)
      } else {
        logger.error('Error:', error)
        reject(error)
      }
    })
  })
}

module.exports = BSConfigLibrary
